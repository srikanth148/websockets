from channels.routing import route


channel_routing = [
    route('websocket.connect', 'src.consumers.ws_connect'),
    route('websocket.disconnect', 'src.consumers.ws_disconnect'),
    route('websocket.receive', 'src.consumers.ws_message'),
]
