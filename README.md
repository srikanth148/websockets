### What is this repository for? ###

* Sample project demonstarting WebSockets implementation in Django
* Version: 0.0.1

### How do I get set up? ###

* `git clone` this repo
* cd to cloned directory `cd websockets`
* install dependencies `pip3 install -r requirements.txt`
* run migrations `python3 manage.py makemigrations && python3 manage.py migrate`
* run server `python3 manage.py runserver`

### Who do I talk to? ###

* arabinda.dora@ggktech.com
